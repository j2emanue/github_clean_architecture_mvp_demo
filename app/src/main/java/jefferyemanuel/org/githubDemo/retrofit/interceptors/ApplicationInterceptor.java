package jefferyemanuel.org.githubDemo.retrofit.interceptors;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class ApplicationInterceptor implements Interceptor {

    @Inject
    public ApplicationInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
//sorry i guess i dont need this, i was going to put the github api key in every header but looks like an open api system  (at least for some calls)
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();
        return chain.proceed(original);
        //HttpUrl url = originalHttpUrl.newBuilder()
        // .addQueryParameter("token", BuildConfig.API_KEY)
        //.build();

//        Request.Builder requestBuilder = original.newBuilder()
//                .url(url);

        //  Request request = requestBuilder.build();
        //return chain.proceed(request);
    }
}
