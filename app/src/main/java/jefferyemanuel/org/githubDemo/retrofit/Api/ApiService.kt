package jefferyemanuel.org.githubDemo.retrofit.Api

import io.reactivex.Observable
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @GET("search/repositories")
    @Headers("Content-Type:application/json")
    fun fetchPopularRepos(@Query("q") query: String, @Query("sort") sort: String = "stars", @Query("order") order: String = "desc"): Observable<PopularRepoModel>

}