package jefferyemanuel.org.githubDemo.ui.mvp.view

import io.reactivex.disposables.Disposable
import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import jefferyemanuel.org.githubDemo.domain.usecases.GetPopularRepoUseCase
import jefferyemanuel.org.githubDemo.domain.usecases.base.DefaultSubscriber
import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.PopularRepoView
import javax.inject.Inject


class PopularRepoPresenter @Inject constructor(var useCase: GetPopularRepoUseCase) : BaseMvpPresenter<PopularRepoView>() {

    fun presentPopularRepoForQuery(query: String) {
        with(useCase) {
            this@with.query = query
            buildObservable().subscribe(object : DefaultSubscriber<PopularRepoModel>() {
                override fun onSubscribe(d: Disposable) {
                    super.onSubscribe(d)
                    showLoading(true)
                }

                override fun onNext(t: PopularRepoModel) {
                    super.onNext(t)
                    t.items = t.items?.filterNotNull() //im not sure if something can come back as null here
                    view?.showPopularRepos(t)
                }

                override fun onError(e: Throwable) {
                    super.onError(e) //can show an error dialog here or retry
                }

                override fun onComplete() {
                    super.onComplete()
                    showLoading(false)
                }
            })
        }
    }

    fun presentItemDetails(item: Item) {
        view?.showItemDetail(item)
    }


}
