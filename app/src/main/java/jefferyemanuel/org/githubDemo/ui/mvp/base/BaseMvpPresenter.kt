package jefferyemanuel.org.githubDemo.ui.mvp.base

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter


abstract class BaseMvpPresenter<V : BaseMvpView> : MvpBasePresenter<V>() {

    fun showLoading(show: Boolean) {
        view?.showLoading(show)
    }
}