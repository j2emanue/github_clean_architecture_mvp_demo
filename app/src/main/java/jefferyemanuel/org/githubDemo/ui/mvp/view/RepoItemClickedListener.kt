package jefferyemanuel.org.githubDemo.ui.mvp.view

import jefferyemanuel.org.githubDemo.domain.model.Item


interface RepoItemClickedListener {
    fun onItemSelected(item: Item)
}