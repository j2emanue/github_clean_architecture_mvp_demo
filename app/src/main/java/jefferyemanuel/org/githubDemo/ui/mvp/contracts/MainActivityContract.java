package jefferyemanuel.org.githubDemo.ui.mvp.contracts;


import org.jetbrains.annotations.NotNull;

import jefferyemanuel.org.githubDemo.domain.model.Item;
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel;
import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpView;

public interface MainActivityContract {

    interface MainActivityView extends BaseMvpView {

        void showPopularRepoScreen();

        void showPopularRepoDetailScreen(@NotNull Item item);
    }

    interface PopularRepoView extends BaseMvpView {

        void showPopularRepos(@NotNull PopularRepoModel t);

        void showItemDetail(@NotNull Item item);
    }

    interface PopularRepoDetailsView extends BaseMvpView {

        void showAvatar(@NotNull String url);

        void showRepoName(@NotNull String name);

        void showRepoDescription(@NotNull String desc);

        void showUrl(@NotNull String url);

        void showScore(double score);
    }
}
