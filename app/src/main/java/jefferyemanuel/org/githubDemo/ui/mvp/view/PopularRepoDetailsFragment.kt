package jefferyemanuel.org.githubDemo.ui.mvp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.githubDemo.R
import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.ui.application.GitHubDemoApplication
import jefferyemanuel.org.githubDemo.ui.base.BaseMvpFragment
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.PopularRepoDetailsView
import kotlinx.android.synthetic.main.fragment_popular_repo_details.*
import java.util.*
import javax.inject.Inject


class PopularRepoDetailsFragment : BaseMvpFragment<PopularRepoDetailsView, PopularRepoDetailsPresenter>(), PopularRepoDetailsView {

    @Inject
    lateinit var mPresenter: PopularRepoDetailsPresenter

    override fun createPresenter(): PopularRepoDetailsPresenter = mPresenter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_popular_repo_details, container, false)
        (requireActivity().applicationContext as GitHubDemoApplication).appComponent.inject(this)
        return rootView
    }

    private var item: Item? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.attachView(this)
        initView()

    }

    private fun initView() {
        item = arguments?.getParcelable<Item>("item")

        item?.let {
            mPresenter.presentAvatar(it)
            mPresenter.presentRepoName(it)
            mPresenter.presentRepoDescription(it)
            mPresenter.presentRepoScore(it)
            mPresenter.presentRepoUrl(it)
        }
    }

    override fun showRepoName(name: String) {
        tv_title.text = name
    }

    override fun showRepoDescription(desc: String) {
        tv_desc.text = desc
    }

    override fun showUrl(url: String) {
        tv_url.text = url
    }

    override fun showScore(score: Double) {
        tv_score.text = String.format(Locale.US,getString(R.string.score),score)
    }

    override fun showAvatar(url: String) {
        iv.setImageURI(url)
    }

    companion object {
        fun newInstance(b: Bundle?): PopularRepoDetailsFragment {
            val fragment = PopularRepoDetailsFragment()
            fragment.arguments = b ?: Bundle()
            return fragment
        }
    }
}
