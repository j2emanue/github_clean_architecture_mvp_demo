package jefferyemanuel.org.githubDemo.ui.mvp.view

import android.os.Bundle
import jefferyemanuel.org.githubDemo.R
import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.ui.application.GitHubDemoApplication
import jefferyemanuel.org.githubDemo.ui.base.BaseMvpActivity
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.MainActivityView
import javax.inject.Inject

class MainActivity : BaseMvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView, RepoItemClickedListener {

    @Inject
    lateinit var mPresenter: MainActivityPresenter

    override fun createPresenter(): MainActivityPresenter = mPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as GitHubDemoApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            mPresenter.presentPopularReposScreen()
        }
    }

    override fun showPopularRepoScreen() {
        //time to show first fragment
        val fm = supportFragmentManager
        val tag = PopularRepoFragment::class.java.name
        val f = fm.findFragmentByTag(tag)
        val transaction = fm.beginTransaction()
        f?.let { transaction.replace(R.id.fragment_content, f, tag) }
                ?: run { transaction.replace(R.id.fragment_content, PopularRepoFragment.newInstance(null), tag) }
        transaction.commit()
    }

    override fun showPopularRepoDetailScreen(item: Item) {
        //time to show first fragment
        val fm = supportFragmentManager
        val tag = PopularRepoDetailsFragment::class.java.name
        val f = fm.findFragmentByTag(tag)
        var b = Bundle()
        b.putParcelable("item", item)
        val transaction = fm.beginTransaction()
        f?.let { transaction.replace(R.id.fragment_content, f, tag) }
                ?: run { transaction.addToBackStack(tag).replace(R.id.fragment_content, PopularRepoDetailsFragment.newInstance(b), tag) }
        transaction.commit()
    }

    override fun onItemSelected(item: Item) {
        mPresenter.presentPopularRepoDetail(item)
    }

}
