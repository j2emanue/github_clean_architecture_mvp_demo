package jefferyemanuel.org.githubDemo.ui.mvp.base;

import com.hannesdorfmann.mosby.mvp.MvpView;

public interface BaseMvpView extends MvpView {
    void showLoading(boolean show);
}
