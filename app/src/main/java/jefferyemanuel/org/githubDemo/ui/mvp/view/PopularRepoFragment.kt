package jefferyemanuel.org.githubDemo.ui.mvp.view

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.githubDemo.R
import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import jefferyemanuel.org.githubDemo.ui.application.GitHubDemoApplication
import jefferyemanuel.org.githubDemo.ui.base.BaseMvpFragment
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.PopularRepoView
import jefferyemanuel.org.githubDemo.ui.mvp.view.adapter.PopularRepoRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_popular_repo.*
import javax.inject.Inject




class PopularRepoFragment : BaseMvpFragment<PopularRepoView, PopularRepoPresenter>(), PopularRepoView {

    var mCallback: RepoItemClickedListener? = null
    @Inject
    lateinit var mPresenter: PopularRepoPresenter

    override fun createPresenter(): PopularRepoPresenter = mPresenter

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            mCallback = activity as RepoItemClickedListener?
        } catch (e: ClassCastException) {
            throw ClassCastException(requireActivity().toString() + " must implement RepoItemClickedListener")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_popular_repo, container, false)
        (requireActivity().applicationContext as GitHubDemoApplication).appComponent.inject(this)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.attachView(this)
        mPresenter.presentPopularRepoForQuery("android")//defaulting to android for this demo
    }

    override fun showPopularRepos(t: PopularRepoModel) {
        t.items?.let {
            rv.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
            val adapter = PopularRepoRecyclerAdapter(it) { item -> mPresenter.presentItemDetails(item) }
            rv.adapter = adapter
        }
    }

    override fun showItemDetail(item: Item) {
       mCallback?.onItemSelected(item)
    }

    companion object {
        const val REQ_CODE_ITEM_DETAIL = 0x9232
        fun newInstance(b: Bundle?): PopularRepoFragment {
            val fragment = PopularRepoFragment()
            fragment.arguments = b ?: Bundle()
            return fragment
        }
    }
}