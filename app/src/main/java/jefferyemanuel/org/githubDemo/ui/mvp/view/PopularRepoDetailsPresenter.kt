package jefferyemanuel.org.githubDemo.ui.mvp.view

import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.PopularRepoDetailsView
import javax.inject.Inject

class PopularRepoDetailsPresenter @Inject constructor() : BaseMvpPresenter<PopularRepoDetailsView>() {

    fun presentAvatar(item: Item) {
        item.owner?.avatar_url?.let { url -> view?.showAvatar(url) }
    }

    fun presentRepoName(item: Item) {
        item.name?.let { view?.showRepoName(it) }

    }

    fun presentRepoDescription(item: Item) {
        item.description?.let { view?.showRepoDescription(it) }
    }

    fun presentRepoUrl(item: Item) {
        item.html_url?.let { view?.showUrl(it) }
    }

    fun presentRepoScore(item: Item) {
        item.score?.let { view?.showScore(it) }
    }
}
