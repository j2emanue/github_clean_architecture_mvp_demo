package jefferyemanuel.org.githubDemo.ui.base

import com.hannesdorfmann.mosby.mvp.MvpActivity
import com.hannesdorfmann.mosby.mvp.MvpPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpView



abstract class BaseMvpActivity <V: BaseMvpView, P: MvpPresenter<V>> : MvpActivity<V, P>(){
    fun showLoading(b:Boolean){} //activity might show one day, for now just stub
}