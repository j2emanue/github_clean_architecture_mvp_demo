package jefferyemanuel.org.githubDemo.ui.mvp.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.githubDemo.R
import jefferyemanuel.org.githubDemo.domain.model.Item
import kotlinx.android.synthetic.main.popular_repo_row.view.*



class PopularRepoRecyclerAdapter constructor(private var dataSource: List<Item?>, var listener: (Item) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        dataSource[position]?.let { (holder as? GitHubItemHolder)?.bind(it,listener) }
    }

    override fun getItemCount() = dataSource.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflatedView = LayoutInflater.from(parent.context)
                .inflate(R.layout.popular_repo_row, parent, false)
        return GitHubItemHolder(inflatedView)
    }

    class GitHubItemHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Item, listener: (Item) -> Unit) = with(itemView) {
            tv_title.text = item.full_name
            tv_desc.text = item.description
            itemView.setOnClickListener { listener(item) }
        }
    }

}