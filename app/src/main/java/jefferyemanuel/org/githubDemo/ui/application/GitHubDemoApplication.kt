package jefferyemanuel.org.githubDemo.ui.application

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import jefferyemanuel.org.githubDemo.dagger.components.AppComponent
import jefferyemanuel.org.githubDemo.dagger.components.DaggerAppComponent
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule
import jefferyemanuel.org.githubDemo.dagger.modules.AppModule
import jefferyemanuel.org.githubDemo.dagger.modules.NetworkModule


open class GitHubDemoApplication : Application() {

    open val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this)).activityModule(ActivityModule())
                .networkModule(NetworkModule())
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        initFresco()
        initStetho()
    }

    private fun initFresco() = Fresco.initialize(this)


    private fun initStetho() = Stetho.initializeWithDefaults(this)

}