package jefferyemanuel.org.githubDemo.ui.base

import com.hannesdorfmann.mosby.mvp.MvpFragment
import com.hannesdorfmann.mosby.mvp.MvpPresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import jefferyemanuel.org.githubDemo.ui.customviews.dialogs.ProgressDialogFragment


abstract class BaseMvpFragment<V : MvpView, P : MvpPresenter<V>> : MvpFragment<V, P>() {

    fun showLoading(show: Boolean) {

        activity?.let {
            it.takeUnless { it.isFinishing }?.also {

                val tag = javaClass.name + "$" + ProgressDialogFragment::class.java.name
                val fm = it.supportFragmentManager
                var loadingView = fm.findFragmentByTag(tag) as? ProgressDialogFragment
                if (loadingView != null) {
                    //i dont care what state this thing had, just a loading dialog
                    fm.beginTransaction().remove(loadingView).commitAllowingStateLoss()
                }

                if (show) {
                    loadingView = ProgressDialogFragment.newInstance(true)
                    loadingView.show(fm, tag)
                }
            }
        }
    }
}