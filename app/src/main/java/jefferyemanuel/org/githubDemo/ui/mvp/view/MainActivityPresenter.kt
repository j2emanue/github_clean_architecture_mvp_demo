package jefferyemanuel.org.githubDemo.ui.mvp.view


import jefferyemanuel.org.githubDemo.domain.model.Item
import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.MainActivityView
import javax.inject.Inject

class MainActivityPresenter @Inject constructor() : BaseMvpPresenter<MainActivityView>() {

    fun presentPopularReposScreen() {
        view?.showPopularRepoScreen()
    }

    fun presentPopularRepoDetail(item: Item) {
        view?.showPopularRepoDetailScreen(item)
    }
}
