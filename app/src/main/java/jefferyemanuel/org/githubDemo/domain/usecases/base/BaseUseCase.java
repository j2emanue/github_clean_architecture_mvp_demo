package jefferyemanuel.org.githubDemo.domain.usecases.base;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public abstract class BaseUseCase {

    public abstract Observable buildUseCaseObservable();

    public Observable buildObservable() {
        return this.buildUseCaseObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
