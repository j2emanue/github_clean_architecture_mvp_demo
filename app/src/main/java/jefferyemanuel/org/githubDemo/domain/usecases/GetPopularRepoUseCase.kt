package jefferyemanuel.org.githubDemo.domain.usecases

import io.reactivex.Observable
import jefferyemanuel.org.githubDemo.data.repositories.DataRepository
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import jefferyemanuel.org.githubDemo.domain.usecases.base.BaseUseCase
import javax.inject.Inject


open class GetPopularRepoUseCase @Inject constructor(var repository: DataRepository) : BaseUseCase() {

    var query: String = ""

    override fun buildUseCaseObservable(): Observable<PopularRepoModel> {
        return repository.fetchPopularRepos(query)
    }
}