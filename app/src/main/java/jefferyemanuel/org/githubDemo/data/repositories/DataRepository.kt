package jefferyemanuel.org.githubDemo.data.repositories

import io.reactivex.Observable
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import jefferyemanuel.org.githubDemo.retrofit.Api.ApiService
import retrofit2.Retrofit
import javax.inject.Inject


class DataRepository @Inject constructor(var mRetrofit: Retrofit) {

    fun fetchPopularRepos(query: String): Observable<PopularRepoModel> {
        val service = mRetrofit.create(ApiService::class.java!!)
        return service.fetchPopularRepos(query)
    }
}