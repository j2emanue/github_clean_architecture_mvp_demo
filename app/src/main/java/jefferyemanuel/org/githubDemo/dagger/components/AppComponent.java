package jefferyemanuel.org.githubDemo.dagger.components;


import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Component;
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule;
import jefferyemanuel.org.githubDemo.dagger.modules.AppModule;
import jefferyemanuel.org.githubDemo.ui.application.GitHubDemoApplication;
import jefferyemanuel.org.githubDemo.ui.mvp.view.MainActivity;
import jefferyemanuel.org.githubDemo.ui.mvp.view.PopularRepoDetailsFragment;
import jefferyemanuel.org.githubDemo.ui.mvp.view.PopularRepoFragment;


@Singleton
@Component(modules = {AppModule.class, jefferyemanuel.org.githubDemo.dagger.modules.NetworkModule.class, ActivityModule.class})
public interface AppComponent {

    void inject(GitHubDemoApplication target);

    void inject(MainActivity target);

    void inject(PopularRepoFragment popularRepoFragment);

    void inject(@NotNull PopularRepoDetailsFragment popularRepoDetailsFragment);
}