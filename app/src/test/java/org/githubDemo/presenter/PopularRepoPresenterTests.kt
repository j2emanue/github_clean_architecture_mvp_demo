package jefferyemanuel.org.githubDemo.ui.mvp.view.presenter

import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import jefferyemanuel.org.githubDemo.domain.model.PopularRepoModel
import jefferyemanuel.org.githubDemo.domain.usecases.GetPopularRepoUseCase
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.MainActivityContract.PopularRepoView
import jefferyemanuel.org.githubDemo.ui.mvp.view.PopularRepoPresenter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PopularRepoPresenterTests {

    @Mock
    lateinit var view: PopularRepoView

    @Mock
    private lateinit var usecase: GetPopularRepoUseCase

    private lateinit var presenter: PopularRepoPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        presenter = PopularRepoPresenter(usecase)
        presenter.attachView(view)
    }

    @Test
    fun popularReposShownSuccessfully() {

        //arrange
        given(usecase.buildObservable()).willReturn(Observable.just(PopularRepoModel()))

        //act
        presenter.presentPopularRepoForQuery("android")

        //assert
        Mockito.verify(view, Mockito.times(1)).showPopularRepos(ArgumentMatchers.any())
    }
}